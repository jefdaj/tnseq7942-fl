with builtins;

#################
# HL experiment #
#################

# These all end in _rN, even the ones with a single rep, for easy merging by column later

{
  hlUndetermined = [ (toPath ./171205_100SR_HS4KA/Undetermined_S0_L001_R1_001.fastq.gz) ];
  hlOutgrowths = rec {

    # no way to treat time0 samples with replicates right? might as well compare each rep to them all
    hl_t0 = [
      (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C1green_S50_L005_R1_001.fastq.gz)
      (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C2green_S51_L005_R1_001.fastq.gz)
      (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C3green_S52_L005_R1_001.fastq.gz)
      (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C4green_S53_L005_R1_001.fastq.gz)
    ];

    ll_r1 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C5green_S54_L005_R1_001.fastq.gz) ];
    ll_r2 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C6green_S55_L005_R1_001.fastq.gz) ];
    ll_r3 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C7green_S56_L005_R1_001.fastq.gz) ];
    ll_r4 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C8green_S57_L005_R1_001.fastq.gz) ];

    nl_r1 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C9green_S58_L005_R1_001.fastq.gz) ];
    nl_r2 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C10green_S59_L005_R1_001.fastq.gz) ];
    nl_r3 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C11green_S60_L005_R1_001.fastq.gz) ];
    # TODO why did this fail? try re-running when time
    # nl_r4 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/C12green_S61_L005_R1_001.fastq.gz) ];

    hl_r1 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/D1green_S62_L005_R1_001.fastq.gz) ];
    hl_r2 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/D2green_S63_L005_R1_001.fastq.gz) ];
    hl_r3 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/D3green_S64_L005_R1_001.fastq.gz) ];
    hl_r4 = [ (toPath ./data/171205_100SR_HS4KA/Lane1345678/Niyogi/D4green_S65_L005_R1_001.fastq.gz) ];

  };
}
