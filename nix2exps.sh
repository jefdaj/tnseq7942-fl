#!/usr/bin/env bash

# Quick script to populate data/experiments.tsv from the nix files I'm actually using to group replicates and data/treatments.tsv
# WARNING: you need to edit it to use the right treatments.tsv if more than one

# replicates version:
# nix_files="$(ls *Fastq*Reps.nix | sort)"
# pooled version:
nix_files="$(ls *FastqFiles.nix | sort)"
sample_indexes="$(grep '=\s*\[' $nix_files  | sort | awk '{print $2}' | grep -v Unde | grep -v '#')"
# treatments="$(grep '=\s*\[' $nix_files | grep -v 't0' | grep -v Unde | grep -v '#' | awk '{print $2}' | sed 's/_r.//g' | sort | uniq)"

header() {
	echo	"SetName	Date_pool_expt_started	Person	Mutant Library	Description	Index	Media	Growth Method	Group	Condition_1	Units_1	Concentration_1	Condition_2	Units_2	Concentration_2"
}

row() {
	time0="$1"
	index="$2"
  [[ "$index" =~ t0 ]] && desc="Time0" || desc="$index"
	echo "SynE_ML6_set13	${time0}	Jeff Johnson	SynE_ML6.3.1	${desc}	${index}	BG11	NA	NA	NA	NA	NA	NA	NA	NA"
}

header
cat data/treatments-pooled.tsv | while read treatment time0; do
  for sample in $sample_indexes; do
    [[ "$sample" =~ "$treatment" ]] || continue
		# echo "treatment: $treatment time0: $time0 sample: $sample"
		row "$time0" "$sample"
  done
done
