with builtins;
with import ./code/1-nixpkgs;
rec {

  ########
  # data #
  ########

  # TODO standardize the names (no underscore issues) and re-run from one experiments table
  # TODO when you have time, re-organize to put these in the data dir?

  # Previous FEBA data from Adam + Morgann
  # TODO have there been any updates since 2017?
  prev = callPackage (import ./data/feba) {};

  # TODO put undetermined in too?
  test = (import ./testFastqFiles.nix).testOutgrowths;

  # replicates pooled to one fastq per treatment
  # TODO put undetermined in too?
  expsPooled = ./data/experiments-pooled.tsv;
  hlPooled   = (import ./hlFastqFiles.nix  ).hlOutgrowths;
  flPooled   = (import ./flFastqFiles.nix  ).flOutgrowths;

  # replicates separate
  # TODO put undetermined in too?
  expsReps = ./data/experiments-reps.tsv;
  hlReps   = (import ./hlFastqFilesReps.nix).hlOutgrowths;
  flReps   = (import ./flFastqFilesReps.nix).flOutgrowths;

  # compares the time0 samples to *their* time0, the original aliquot
  expsT0Pooled = ./data/experiments-time0.tsv;
  expsT0Reps   = ./data/experiments-time0-reps.tsv;

  allPooled = test // hlPooled // flPooled;
  allReps   = test // hlReps   // flReps;

  t0Pooled  = {
    inherit (test) hepesMinus;
    inherit (hlPooled) hlt0;
    inherit (flPooled) fl12t0 fl34t0 fl56t0 pbrft0;
  };
  t0Reps  = {
    inherit (test) hepesMinus;
    t0_r1 = hlReps.hl_t0;
    t0_r2 = flReps.fl12_t0;
    t0_r3 = flReps.fl34_t0;
    t0_r4 = flReps.fl56_t0;
    # t0_r5 = flReps.pbrf_t0; # TODO remove?
  };

  #################
  # FEBA analysis #
  #################

  febaPython = pythonPackages.python.withPackages (ps: with ps; [pathlib biopython]);

  # one function to do all the standard steps that work on a single treatment
  # takes a set where the attibute names are treatments and the values are lists of fastq files,
  # and one name to actually operate on
  # returns a count table for that treatment
  countCodes = percentReads: treatments: treatmentName:
    let
      index      = treatmentName;
      fastqs     = getAttr treatmentName treatments;
      fastqfix   = callPackage ./code/2-feba/1-fastqfix   { inherit index fastqs percentReads; python=febaPython; };
      multicodes = callPackage ./code/2-feba/2-multicodes { inherit feba index fastqfix; };
    in
      multicodes;

  # and a bigger function to do that + the rest of the FEBA pipeline
  febaBarseq = { feba, prev, allOutgrowths, exps, percentReads }:
    let
      allCodeTables = map ( countCodes percentReads allOutgrowths ) ( attrNames allOutgrowths );
      allCombinebarseq = callPackage ./code/2-feba/3-combinebarseq {
        inherit feba prev; codeTables = allCodeTables;
      };
      allBarseqr = callPackage ./code/2-feba/4-barseqr {
        inherit feba exps;
        syne = prev;
        combinebarseq = allCombinebarseq;
      };
    in allBarseqr;

  feba-results = {
    # all experiments (replicates left separate for error bars)
    feba-reps-t0   = febaBarseq { inherit feba prev; exps = expsT0Reps; allOutgrowths = t0Reps ; percentReads = 100; };
    feba-reps-exps = febaBarseq { inherit feba prev; exps = expsReps  ; allOutgrowths = allReps; percentReads = 100; };
  
    # all experiments (replicates pooled for better FEBA scores)
    feba-pooled-t0   = febaBarseq { inherit feba prev; exps = expsT0Pooled; allOutgrowths = t0Pooled ; percentReads = 100; };
    feba-pooled-exps = febaBarseq { inherit feba prev; exps = expsPooled  ; allOutgrowths = allPooled; percentReads = 100; };
  
    # try subsampling reads to see if the HL experiment gets similarly noisy with fewer:
    feba-hl-reps-100 = febaBarseq { inherit feba prev; exps = expsReps; allOutgrowths = hlReps; percentReads = 100; };
    feba-hl-reps-050 = febaBarseq { inherit feba prev; exps = expsReps; allOutgrowths = hlReps; percentReads =  50; };
    feba-hl-reps-025 = febaBarseq { inherit feba prev; exps = expsReps; allOutgrowths = hlReps; percentReads =  25; };
    feba-hl-reps-010 = febaBarseq { inherit feba prev; exps = expsReps; allOutgrowths = hlReps; percentReads =  10; };
    feba-hl-reps-005 = febaBarseq { inherit feba prev; exps = expsReps; allOutgrowths = hlReps; percentReads =   5; };
    feba-hl-reps-001 = febaBarseq { inherit feba prev; exps = expsReps; allOutgrowths = hlReps; percentReads =   1; };

    # and again with replicates pooled
    feba-hl-pooled-100 = febaBarseq { inherit feba prev; exps = expsPooled; allOutgrowths = hlPooled; percentReads = 100; };
    feba-hl-pooled-050 = febaBarseq { inherit feba prev; exps = expsPooled; allOutgrowths = hlPooled; percentReads =  50; };
    feba-hl-pooled-025 = febaBarseq { inherit feba prev; exps = expsPooled; allOutgrowths = hlPooled; percentReads =  25; };
    feba-hl-pooled-010 = febaBarseq { inherit feba prev; exps = expsPooled; allOutgrowths = hlPooled; percentReads =  10; };
    feba-hl-pooled-005 = febaBarseq { inherit feba prev; exps = expsPooled; allOutgrowths = hlPooled; percentReads =   5; };
    feba-hl-pooled-001 = febaBarseq { inherit feba prev; exps = expsPooled; allOutgrowths = hlPooled; percentReads =   1; };
  };

  ###################
  # custom analyses #
  ###################

  genesTsv = toPath ./data/genes.tsv;
  keggDir  = toPath ./code/kegg; # TODO remove?

  # oldAnalysis = callPackage ./code/3-analysis {
  #   inherit testFastqFiles testCodeTables testCombinebarseq;
  #   inherit keggDir genesTsv;
  #   inherit flFastqFiles flCodeTables flCombinebarseq;
  #   inherit feba-results;
  # };

  # TODO include the code you used to generate this!
  clustData = {
    reps = ./data/clust-replicates.txt;
    # TODO only need one of these right?
    # geneCounts = ./data/clust-genecounts-narm.tsv;
    counts = ./data/clust-genecounts.tsv; # TODO adjust name to match geneCounts?
  };
  # TODO have to upgrade python for portalocker?
  # TODO move to code/1-nixpkgs
  # clustApp = callPackage ./code/3-analysis/clust {};

  # TODO remove or make explicit here:
  # annotation.tsv
  # biocyc-gene-info.tsv
  # papers.tsv
  # ps-genes-not-analyzed.tsv
  # treatments-pooled.tsv
  # treatments-reps.tsv

  # TODO separate packages from main.R, tables-and-figures.R:
  # milkyway
  # rsquared
  # pca
  # heatmaps
  # clust
  # feba (later)

  feba-stats  = callPackage ./code/3-analysis/feba-stats  { inherit feba-results; };
  paperBlastTags = ./data/paper-blast/tags; # TODO paths only?
  paper-blast = callPackage ./code/3-analysis/paper-blast { inherit feba-results paperBlastTags; };
}
