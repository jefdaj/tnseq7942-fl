suppressPackageStartupMessages(require(ggpmisc))
suppressPackageStartupMessages(require(dplyr))
suppressPackageStartupMessages(require(ggplot2))

# source('compare.R')

plain_theme <- function(p)
  p + theme_bw() +
      theme(legend.position = "none",
            panel.border = element_blank(),
            panel.grid.major = element_blank(),
            panel.grid.minor = element_blank(),
            axis.line = element_line(colour = "black"))

# based on https://stackoverflow.com/a/35140066
# note you have to name the variables x and y first
# like this: plot_rsquared(mutate(df2, x=logratio_mean_fl1, y=logratio_mean_pbr_fl2))
plot_rsquared <- function(df) {
  my.formula <- y ~ x
  p <- df %>% ggplot(aes(x = x, y = y)) +
          geom_point(alpha=0.3) +
          geom_smooth(method = "lm", se=FALSE, color="red", formula = my.formula) +
          stat_poly_eq(formula = my.formula, 
                       aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")), 
                       parse = TRUE)
  return(plain_theme(p))
}

# compare_all_treatments(df2, function(t1, t2, path) {
#   p <- plot_rsquared(mutate_(df2, x=t1, y=t2))
#   ggsave(p, file=path)
# })

save_rsquared <- function(df, t1, t2, f) {
  # f <- file.path(outDir, paste0('tnseq7942-', xvar, '-vs-', yvar, '-', cutoff, '.png'))
  # if (file.exists(f)) {
    # cat(paste0('skipping ', f, '\n'))
  # } else {
  #p <- plot_rsquared(df)
  #ggsave(filename=f, p, width=6, height=6)
  p <- plot_rsquared(mutate_(df, x=t1, y=t2))
  cat(paste0('saving ', f, '\n'))
  ggsave(p, file=f, width=6, height=6)
  # }
}
