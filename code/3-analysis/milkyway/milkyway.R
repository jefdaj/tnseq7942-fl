suppressPackageStartupMessages(require(dplyr))
suppressPackageStartupMessages(require(ggplot2))
suppressPackageStartupMessages(require(ggrepel))

passes_cutoff <- function(xvar, xsd, yvar, ysd, cutoff)
  abs( yvar-xvar ) >= cutoff

passes_cutoff_worstcase <- Vectorize(function(xvar, xsd, yvar, ysd, cutoff) {
  # print(c(xvar,xsd,yvar,ysd,cutoff))
  if (!(abs(yvar-xvar) >= cutoff)) {
    # the point itself doesn't pass
    return(FALSE)

  # this breaks cases like HliC in HL/LL where the error bars cross an axis :(
  # } else if ((!is.na(xsd) && (xsd >= abs(xvar))) || (!is.na(ysd) && (ysd >= abs(yvar)))) {
  #   return(FALSE)
  # this is an attempt to remove the "giant error bar" cases like 1701 without that side effect:
  } else if ((!is.na(xsd) && abs(xsd) > cutoff) | (!is.na(ysd) && abs(ysd) > cutoff)) {
    return(FALSE)

  } else if (yvar < xvar) {
    # print('lower right')
    # lower right quadrant; worst case is upper left of error cross
    if (!is.na(xsd)) xvar <- xvar - xsd
    if (!is.na(ysd)) yvar <- yvar + ysd
  } else {
    # print('upper left')
    # upper left quadrant; worst case is lower right of error cross
    if (!is.na(xsd)) xvar <- xvar + xsd
    if (!is.na(ysd)) yvar <- yvar - ysd
  }
  # print(c(xvar, yvar, cutoff))
  return(abs(yvar-xvar) >= cutoff)
})

plain_theme <- function(p)
  p + theme_bw() +
      theme(legend.position = "none",
            panel.border = element_blank(),
            panel.grid.major = element_blank(),
            panel.grid.minor = element_blank(),
            axis.line = element_line(colour = "black"))

plot_milkyway <- function(df, m, xvar, yvar, remove, cutoff) {
  # print(colnames(df))
  # print(c(xvar, yvar, cutoff))

  # remove genes in the preexising blacklist (will be plotted in red)
  removed    <- df %>% filter( locusId %in% remove)
  df         <- df %>% filter(!locusId %in% remove)
  # print(removed)

  # TODO detect if these don't exist
  xsd <- paste0('sd_'  , xvar)
  ysd <- paste0('sd_'  , yvar)
  # print(df)
  have_sd <- xsd %in% colnames(df) & !is.null(df[[xsd]]) # & nrow(df[!is.na(df[[xsd]]),]) > 0
  # print(have_sd)

  # this is pretty rediculous, but see:
  # https://stackoverflow.com/a/27197858
  # is_foreground <- lazyeval::interp(quote(abs(yvar-xvar)>=cutoff), yvar = as.name(yvar), xvar=as.name(xvar))
  # is_background <- lazyeval::interp(quote(abs(yvar-xvar)< cutoff), yvar = as.name(yvar), xvar=as.name(xvar))

  if (have_sd) {
    foreground <- df %>%
      mutate_(ymin=lazyeval::interp(quote(yvar-ysd), yvar=as.name(yvar), ysd=as.name(ysd)),
              ymax=lazyeval::interp(quote(yvar+ysd), yvar=as.name(yvar), ysd=as.name(ysd)),
              xmin=lazyeval::interp(quote(xvar-xsd), xvar=as.name(xvar), xsd=as.name(xsd)),
              xmax=lazyeval::interp(quote(xvar+xsd), xvar=as.name(xvar), xsd=as.name(xsd))) %>%

      # this works, but doesn't take standard deviations into account
      # filter_(lazyeval::interp(quote(abs( yvar-xvar )>=cutoff),
      #                          yvar = as.name(yvar), xvar=as.name(xvar), xsd=as.name(xsd), ysd=as.name(ysd)))

      filter_(lazyeval::interp(quote(passes_cutoff_worstcase(xvar, xsd, yvar, ysd, cutoff)),
                               yvar = as.name(yvar), xvar=as.name(xvar), xsd=as.name(xsd), ysd=as.name(ysd)))
  } else {
    foreground <- df %>% filter_(lazyeval::interp(quote(abs(yvar-xvar)>=cutoff), yvar = as.name(yvar), xvar=as.name(xvar)))
  }

  # print(foreground %>% nrow)
  background <- df %>% filter(!locusId %in% foreground$locusId)
  # print(background %>% nrow)

  have_fg <- nrow(foreground) > 0

  # add foreground labels from metadata
  m <- transmute(m, locusId=locusId, label=Genes)
  foreground <- left_join(foreground, m, by='locusId') %>% mutate(label=sub('SYNPCC7942_', '', label))

  # scale the plot to fit while keeping the aspect ratio square
  if (have_fg & have_sd) {
    smin <- min(min(foreground$xmin), min(foreground$ymin))
    smax <- max(max(foreground$xmax), max(foreground$ymax))
  } else if (have_fg) {
    smin <- min(min(foreground[[xvar]]), min(foreground[[yvar]]))
    smax <- max(max(foreground[[xvar]]), max(foreground[[yvar]]))
  } else {
    smin <- min(min(background[[xvar]]), min(background[[yvar]]))
    smax <- max(max(background[[xvar]]), max(background[[yvar]]))
  }

  # add labels explaining the quadrants
  # from https://stackoverflow.com/a/32290778
  annotations <- data.frame(
    xpos = c(-Inf, Inf),
    ypos = c( Inf,-Inf),
    annotateText = c(paste(" relative defect in", xvar),
                     paste("relative defect in", yvar)),
    hjustvar = c(0, 1) ,
    vjustvar = c(1,-1)) #<- adjust

  p <- ggplot(background, aes_(x=as.name(xvar), y=as.name(yvar))) +
          # show axes
          geom_hline(aes(yintercept=0), alpha=0.3) +
          geom_vline(aes(xintercept=0), alpha=0.3) +
          # show 0 and +/- cutoff
          # geom_abline(aes(slope=1, intercept=0      ), alpha=0.5) +

          geom_abline(aes(slope=1, intercept=cutoff ), color='lightblue') +
          geom_abline(aes(slope=1, intercept=(0-cutoff)), color='lightblue') +

          # annotations
          geom_text(data=annotations,aes(x=xpos,y=ypos,hjust=hjustvar,vjust=vjustvar,label=annotateText), color="lightblue") +

          # add plain background points
          geom_point(alpha=0.3) +
          # grey out removed genes
          geom_point(data=removed, color='red')

  if (have_fg & have_sd) {
    p <- p + geom_errorbar( data=foreground, aes(ymin=ymin, ymax=ymax), color='blue', width=0) +
             geom_errorbarh(data=foreground, aes(xmin=xmin, xmax=xmax), color='blue', height=0)
  }
  if (have_fg) {
    p <- p + geom_label_repel(data=foreground, aes_string(x=xvar, y=yvar, label='label'))
  }

  p <- p + 
          # add foreground points with more fanciness
          geom_point(data=foreground, color='blue', size=2) +
          scale_x_continuous(breaks=seq(-10,10,1), limits=c(smin,smax)) +
          scale_y_continuous(breaks=seq(-10,10,1), limits=c(smin,smax)) +
          xlab(xvar) +
          ylab(yvar)

  return(plain_theme(p))
}

save_milkyway <- function(df, m, xvar, yvar, f, remove, cutoff) {
  # f <- file.path(outDir, paste0('tnseq7942-', xvar, '-vs-', yvar, '-', cutoff, '.png'))
  # if (file.exists(f)) {
    # cat(paste0('skipping ', f, '\n'))
  # } else {
  p <- plot_milkyway(df, m, xvar, yvar, remove, as.numeric(cutoff))
  if (!is.null(p)) {
    cat(paste0('saving ', f, '\n'))
    ggsave(filename=f, p, width=6, height=6)
  }
  # }
}
