{ stdenv
, rWrapper
, rPackages
, feba-results
, paperBlastTags
}:

let
  myR = rWrapper.override { packages = with rPackages; [
    tidyverse
    ggrepel
  ];};

in stdenv.mkDerivation (feba-results // {
  inherit paperBlastTags;

  name = "tnseq7942-paper-blast";
  buildInputs = [
    myR
  ];
  src = ./.;
  installPhase = ''
    source ${stdenv}/setup
    mkdir -p $out
    cd $out
    Rscript $src/paper-blast.R
  '';
})
