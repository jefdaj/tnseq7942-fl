#!/usr/bin/env bash

# cmd="Rscript -e \"library(rmarkdown); render('index.Rmd')\""

source ${stdenv}/setup
cd $TMPDIR
mkdir -p $out
cp ${src}/*.R $TMPDIR/

cmd="Rscript ${src}/tables-and-figures.R $(realpath $TMPDIR) ${flBarseqr} $out"
echo "$cmd" 2>&1 | tee builder.log
eval "$cmd" 2>&1 | tee -a builder.log

cp *.R *.png *.tsv $out
