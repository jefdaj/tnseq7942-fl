list_treatments <- function(df)
  sub('sd_', '', colnames(select(df, -locusId, -desc)))

# pathFn should take 2 arguments: t1 and t2
# plotFn should take 3 arguments: t1, t2, and path, and save something to the path
compare_all_treatments <- function(df, pathFn, plotFn) {
  ts <- list_treatments(df)
  # print(ts)
  invisible(lapply(ts, function(t1)
    lapply(ts, function(t2) {
      if (t1 != t2) {

        # sort treatments to avoid plotting the same ones against each other twice
        tmp <- sort(c(t1, t2))
        t1 <- tmp[1]
        t2 <- tmp[2]

        p <- pathFn(t1, t2)
        # print(p)
        if (!dir.exists(dirname(p))) dir.create(dirname(p))
        if (!file.exists(p)) tryCatch(plotFn(df, t1, t2, p), error=function(e) cat(paste0('FAILED ', p, '\n')))
        # if (!file.exists(p)) plotFn(df, t1, t2, p)
      }
    })
  ))
}
