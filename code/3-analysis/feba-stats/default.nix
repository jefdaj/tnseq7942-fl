{ stdenv
, rWrapper
, rPackages
, feba-results
}:

let
  myR = rWrapper.override { packages = with rPackages; [
    tidyverse
    ggrepel
  ];};

in stdenv.mkDerivation (feba-results // {
  name = "tnseq7942-feba-stats";
  buildInputs = [
    myR
  ];
  src = ./.;
  installPhase = ''
    source ${stdenv}/setup
    mkdir -p $out
    cd $out
    Rscript $src/feba-stats.R
  '';
})
