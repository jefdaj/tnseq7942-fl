with builtins;
# with import ../01-nixdeps;
with import /home/jefdaj/nixpkgs {};
let
  # testFastqFiles = ;
  # testCodeTables = ;
  # testCombinebarseq = ;
  # keggDir = ;
  # flFastqFiles = ;
  # flCodeTables = ;
  # flCombinebarseq = ;
  genesTsv      = toPath ../../data/genes.tsv;
  barseqrPooled = toPath /home/user/tnseq7942-fl/barseqr-pooled-missing-nl;
  barseqrReps   = toPath /home/user/tnseq7942-fl/barseqr-reps;

in callPackage ./default.nix {
  # inherit testFastqFiles testCodeTables testCombinebarseq;
  # inherit keggDir genesTsv;
  # inherit flFastqFiles flCodeTables flCombinebarseq flBarseqr;
  inherit genesTsv barseqrPooled barseqrReps;
}
