{ stdenv, rWrapper, rPackages, inotifyTools, pandoc, which, gzip, bc
# , clust

, testFastqFiles
, testCodeTables
, testCombinebarseq
, keggDir
, genesTsv
, flFastqFiles
, flCodeTables
, flCombinebarseq
, febaResults

}:

# TODO use the fl ones!

let
  myR = rWrapper.override { packages = with rPackages; [
    dplyr
    ggplot2
    ggpmisc
    ggrepel
    knitr
    markdown
    rmarkdown
    tidyr
    tidyverse
    purrr
    data_table
    pathview # TODO put back when have fast internet
    clusterProfiler # TODO put back when have fast internet

    DESeq2
    pheatmap
    dendextend
    dendsort
    RColorBrewer
    viridis

    UpSetR
    VennDiagram

    factoextra
  ];};

in stdenv.mkDerivation rec {
  name = "tnseq7942v2-analysis";
  buildInputs = [
    inotifyTools # TODO remove?
    pandoc # TODO remove?
    # clust # TODO fix dependencies (portalocker needs back-porting?)
    myR
    which
    gzip
    bc
  ];
  # inherit testFastqFiles testCodeTables testCombinebarseq;
  # inherit keggDir growthCurves genesTsv;
  # inherit flFastqFiles flCodeTables flCombinebarseq flBarseqr;
  inherit genesTsv;

  # auto-update index.html in the src dir itself when coding
  # srcPath = builtins.toPath ./.;
  # shellHook = ./shell.sh;

  # build final output in a separate nix-store path
  src = ./.;
  builder = ./builder.sh;
}
