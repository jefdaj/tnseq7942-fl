suppressPackageStartupMessages(require(tidyverse))
suppressPackageStartupMessages(require(ggrepel))

# TODO put in a proper legend

volcano_plot <- function(df, outpath, title, x_label, y_label, color_threshold, x_max, y_max) {
  # print(df)
  if (file.exists(outpath)) {
    # cat(paste0('skipping ', outpath, '\n'))
  } else {

    # split genes by whether they pass the t-like threshold
    df_below <- filter(df, label == '', y <  color_threshold)
    df_above <- filter(df, label == '', y >= color_threshold)
    df_labeled <- filter(df, label != '')
  
    p <- ggplot(df_below, aes(x=x, y=y)) +

      geom_point(alpha=0.2) +
      geom_point(data=df_above) +
      geom_point(data=df_labeled, aes(color=color)) +
      # geom_label_repel(data=df_labeled, aes_string(label='label', alpha=0.5)) +

      ggtitle(title) + xlab(x_label) + ylab(y_label) +
      scale_x_continuous(limits=c(-x_max, x_max), breaks=seq(-x_max, x_max, by=1)) +
      scale_y_continuous(limits=c(0     , y_max), breaks=seq(0     , y_max, by=5)) +
      theme_bw() +
      theme(legend.position = "none",
            panel.grid.major = element_blank(),
            panel.grid.minor = element_blank())

    cat(paste0("saving volcano plot: ", outpath, "\n"))
    ggsave(p, filename=outpath, width=8, height=8)
  }

}
