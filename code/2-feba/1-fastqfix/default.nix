{ stdenv, python, index, fastqs, percentReads }:

stdenv.mkDerivation {
  name = "tnseq7942-fastqfix";
  src = ./.;
  buildInputs = [ python ];
  inherit index fastqs python percentReads;
  builder = ./builder.sh;
}
