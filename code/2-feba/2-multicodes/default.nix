{ stdenv, feba, index, fastqfix }:

stdenv.mkDerivation {
  name = "tnseq7942-multicodes";
  src = ./.;
  buildInputs = [ feba ];
  inherit index fastqfix;
  builder = ./builder.sh;
}
