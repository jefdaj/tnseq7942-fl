From the FEBA readme:

MultiCodes.pl identifies the barcode in each read and makes a table of
how often it saw each barcode. It can also demultiplex reads if using
the older primers with inline demultiplexing and no separate index
read.
