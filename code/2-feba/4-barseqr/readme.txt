From the FEBA readme:

BarSeqR.pl combines multiple lanes of barseq output with the genes
table to make a single large table, and then uses the R code in FEBA.R
to estimate the fitness of each gene in each experiment. It produces a
mini web site with data tables and quality assessment plots. It
requires metadata about the experiments (usually FEBA_BarSeq.tsv) and
information about the GC content of each gene (genes.GC -- this can be
produced from a normal genes table with RegionGC.pl).

In FEBA_BarSeq.tsv, the Date_pool_expt_started field specifies what
date the experiment was started on, and experiment(s) with
Description=Time0 are the control sample(s) for that set of
experiments. If you need to set up your controls in a different way,
use a different Date_pool_expt_started (any character string is
allowed) or use BarSeqTest.pl. SetName and Index indicate which reads
correspond to that sample.

If you want to use the cgi scripts to view the results, then the
metadata table must include all of these fields: SetName
Date_pool_expt_started Person "Mutant Library" Description Index Media
"Growth Method" Group Condition_1 Units_1 Concentration_1 Condition_2
Units_2 Concentration_2.

The results of BarSeqR.pl may change as data is added, as this alters
which strains are considered abundant enough to include in the
analysis. You can get around this by using the SaveStrainUsage.pl
script after you process your data. This will record which strains
were used for an analysis, and if these files are saved to the
organism directory, then BarSeqR.pl will use this information.  (You
can turn this behavior of BarSeqR.pl off by setting the
FEBA_NO_STRAIN_USAGE environment variable.)
