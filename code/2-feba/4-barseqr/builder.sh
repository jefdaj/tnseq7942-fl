#!/usr/bin/env bash

source ${stdenv}/setup
mkdir -p $out

# TODO handle more sets?
cmd="cp ${combinebarseq}/all.poolcount $out/SynE_ML6_set13.poolcount"
echo "$cmd" && eval "$cmd" 2>&1 | tee $out/builder.log

cmd="BarSeqR.pl -org SynE -indir $out -outdir $out"
cmd="$cmd -exps ${exps}"
cmd="$cmd -genes ${syne}/genes"
cmd="$cmd -pool ${syne}/pool"
cmd="$cmd SynE_ML6_set13"
echo "$cmd" && eval "$cmd" 2>&1 | tee $out/builder.log
