#!/usr/bin/env bash

source ${stdenv}/setup
mkdir -p $out
# TODO name the pool something other than all? does it matter?
codes="$(for d in ${codeTables}; do ls $d/*.codes; done | xargs echo)"
cmd="combineBarSeq.pl $out/all ${prev}/pool $codes"
cmd="$cmd 2>&1 | tee $out/builder.log"
echo "$cmd" && eval "$cmd"
