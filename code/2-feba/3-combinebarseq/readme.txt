From the FEBA readme:

combineBarSeq.pl merges the table of counts from MultiCodes.pl with
the pool definition from DesignRandomPool.pl to make a table of how
often each strain was seen.

In my case, I think there will be lots of smaller count tables?
