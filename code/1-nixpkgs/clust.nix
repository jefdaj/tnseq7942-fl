# {stdenv, lib, fetchurl, python, makeWrapper, writeScript}:
# with pkgs;
with import <nixpkgs> {};
# with import /home/jefdaj/nixpkgs {};

let
  myPython = python.withPackages (ps: with ps; [
    numpy
    scipy
    matplotlib
    scikitlearn
    pandas
    joblib
    portalocker # comes in current nixpkgs but not the old tnseq pinned one
  ]);
  myPython2 = myPython.override (args: { ignoreCollisions = true; });

in stdenv.mkDerivation rec {
  name = "clust-${version}";
  version = "1.10.7";
  src = fetchurl {
    url = "https://github.com/BaselAbujamous/clust/releases/download/v${version}/clust-${version}.tar.gz";
    sha256 = "1mn7bz33d2nr0i4x96ywl0qz0myvbhsdz42c03vjp7gqz8z5pgw9";
  };
  buildInputs = [ myPython2 makeWrapper ];
  builder = writeScript "builder.sh" ''
    #!/usr/bin/env bash
    source ${stdenv}/setup
    cd $TMPDIR
    tar xvzf $src
    mkdir -p $out/bin
    cp -r clust-${version}/clust.py clust-${version}/clust $out/bin
    chmod +x $out/bin/clust.py
    wrapProgram $out/bin/clust.py \
      --prefix PATH : "${lib.makeBinPath [ myPython2 ]}" \
      --prefix PYTHONPATH : "$out/bin"
  '';
}
