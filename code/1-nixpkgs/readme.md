nixpkgs
=======

These are dependencies I packaged that might be useful to others outside my
tnseq project. I plan to fix them up a bit and send them upstream as pull
requests to [nixpkgs][1] as soon as there's time.

[1]: https://github.com/NixOS/nixpkgs
