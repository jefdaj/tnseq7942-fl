# fetch my pinned nixpkgs for reproducibility
# to update the the sha256sum, use nix-prefetch-url --unpack
# (see https://github.com/NixOS/nix/issues/1381#issuecomment-300755992)
# with import (fetchTarball {
#   url = "https://github.com/jefdaj/nixpkgs/archive/2017-10-02_feba.tar.gz";
#   sha256 = "1lj3paw9z0n8v1dk8nxmnd7i0z209746cyz19vsadkswd87x7ipm";
# }) {};
# with import ../../../nixpkgs {};

let
  # fetch my pinned nixpkgs for reproducibility.
  # see https://vaibhavsagar.com/blog/2018/05/27/quick-easy-nixpkgs-pinning/
  # use this instead to try to build it with your system's current nixpkgs:
  # pkgs = import <nixpkgs> {};
  # to update the the sha256sum:
  # nix-prefetch-url --unpack https://github.com/jefdaj/nixpkgs/archive/2017-10-02_feba.tar.gz
  pkgs = let
    inherit (import <nixpkgs> {}) stdenv fetchFromGitHub;
    in import (fetchFromGitHub {
      owner  = "jefdaj";
      repo   = "nixpkgs";
      rev = "63588cf0bd9247352ed0112d3197788dca96d5f1"; # 2017-10-02_feba
      sha256 = "0hl9rndphxzmvi6qd44nbd51kk8d9q69ccgzklna0qz0agxy4b6i";
    }) {};

  # this is the main thing that will probably break on newer nixpkgs releases
  myPerlPackages = with pkgs.perlPackages; pkgs.perlPackages // {
    BioPerl = pkgs.callPackage ./bioperl.nix {};
  };

in pkgs // {
  perlPackages = myPerlPackages;
  feba  = pkgs.callPackage ./feba.nix { perlPackages = myPerlPackages; };
  clust = pkgs.callPackage ./clust.nix { inherit (pkgs) python; };
}
