tnseq7942
=========

A good way to build it:

```.bash
nix-prefetch-url https://launchpad.net/ubuntu/+archive/primary/+sourcefiles/tiff/4.0.8-4/tiff_4.0.8-4.debian.tar.xz
nix-build --verbose -j$(nproc) --option build-use-chroot false --keep-going
```

The no chroot thing is important because all the FASTQ files are specified as paths
to avoid copying them to the Nix store.

The keep going thing prevents it from killing the remaining fastqfix and multicodes steps if your later analysis needs debugging.
