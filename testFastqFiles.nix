with builtins;

###################################
# miseq to test initial outgrowth #
###################################

{
  # miseq test fastq files grouped by treatment for barseq
  # and the ungrouped, raw files for counting total reads
  # these will be used in the first part of the analysis, but just for general stats (no barseqr)
  testUndetermined = [ (toPath ./data/170927_150SR_MS2/Undetermined_S0_L001_R1_001.fastq.gz) ];
  testOutgrowths = rec {
    hepesPlus  = [ (toPath ./data/170927_150SR_MS2/Niyogi/tnseq2_A9_S1_L001_R1_001.fastq.gz) ];
    hepesMinus = [ (toPath ./data/170927_150SR_MS2/Niyogi/tnseq2_A10_S2_L001_R1_001.fastq.gz) ];
    hepesBoth  = concatLists [ hepesPlus hepesMinus ];
  };
  testFastqFiles = concatLists [
    testOutgrowths.hepesPlus
    testOutgrowths.hepesMinus
    testUndetermined
  ];
}
