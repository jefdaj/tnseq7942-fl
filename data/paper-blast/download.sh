#!/usr/bin/env bash

# download PAPER-BLAST pages
# awk '{print $1}' ../genes.tsv | tail -n +2 | while read locusid; do
#   sleep $(shuf -i 3-10 -n 1)
#   url="http://papers.genomics.lbl.gov/cgi-bin/litSearch.cgi?query=${locusid}&Search=Search"
#    file="${locusid}.html"
#   [[ -a "$file" ]] && continue
#   curl -s "$url" > "$file" && echo "success: ${locusid}" || echo "FAILED: ${locusid}"
# done

# convert to text for grepping
# TODO grep better without the pretty?
# for f in *.html; do
#   html2text -style pretty "$f" > "${f/html/txt}"
# done

# tag genes whose homologs match relevant keywords
# (this is preliminary and rough, of course)
mkdir -p tags
cat patterns.txt | while read line; do
  echo "$line" | while IFS=: read -r name ptns; do
    [[ "$name" =~ "#" ]] && continue
    file="tags/${name}.txt"
    [[ -a "$file" ]] && echo "skipping $file" && continue
    echo "creating $file"
    IFS=";"
    for ptn in $ptns; do
      for txt in Synpcc7942_*.txt; do

        # print matches for debugging:
        # egrep -i "$ptn" "$txt" &>/dev/null && echo "$ptn: $txt" # | cut -d'.' -f1
      # done

        # send matches to files
        egrep -i "$ptn" "$txt" &>/dev/null && echo "$txt" | cut -d'.' -f1
      done | sort | uniq > "$file"

    done
  done
done
